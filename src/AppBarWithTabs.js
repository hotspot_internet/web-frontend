import React from 'react';
import { makeStyles, Theme } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import PropTypes from 'prop-types';

interface TabPanelProps {
  children: React.ReactNode;
  index: any;
  value: any;
}

function TabPanel(props: TabPanelProps) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      // eslint-disable-next-line react/jsx-props-no-spreading
      {...other}
    >
      {value === index && (
        <Box p={3}>
          <Typography component="span">{children}</Typography>
        </Box>
      )}
    </div>
  );
}

function a11yProps(index: any) {
  return {
    id: `simple-tab-${index}`,
    'aria-controls': `simple-tabpanel-${index}`,
  };
}

const useStyles = makeStyles((theme: Theme) => ({
  root: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.paper,
  },
}));

class TabInfo {
  tabName: String
  tabComponent: React.Component

  constructor(tabName: String, tabComponent: React.Component) {
    this.tabName = tabName;
    this.tabComponent = tabComponent;
  }
}

function AppBarWithTabs(props) {
  const classes = useStyles();
  const [value, setValue] = React.useState(0);
  const { tabs } = props;

  const handleChange = (event: React.ChangeEvent<{}>, newValue: number) => {
    setValue(newValue);
  };

  return (
    <div className={classes.root}>
      <AppBar position="static">
        <Tabs value={value} onChange={handleChange} aria-label="simple tabs example">
          {tabs.map((tab: TabInfo, index: number) => (
            // eslint-disable-next-line react/jsx-props-no-spreading
            <Tab label={tab.tabName} {...a11yProps(index)} key={tab.tabName} />
          ))}
        </Tabs>
      </AppBar>
      {tabs.map((tab: TabInfo, index: number) => {
        const TabComponent = tab.tabComponent;
        return (
          <TabPanel value={value} index={index} key={tab.tabName}>
            <TabComponent />
          </TabPanel>
        );
      })}
    </div>
  );
}

AppBarWithTabs.propTypes = {
  tabs: PropTypes.arrayOf(TabInfo).isRequired,
};

export { TabInfo, AppBarWithTabs };
