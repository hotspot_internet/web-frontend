export default class NetworkUsage {
  inBytes: bigint
  outBytes: bigint

  constructor(inBytes: bigint, outBytes: bigint) {
    this.inBytes = inBytes;
    this.outBytes = outBytes;
  }

  aggregateBytes(): number {
    return this.inBytes + this.outBytes;
  }

  static sumNetworkUsages(networkUsages: [NetworkUsage]): NetworkUsage {
    let inBytes = 0;
    let outBytes = 0;

    networkUsages.forEach((usage) => {
      inBytes += usage.inBytes;
      outBytes += usage.outBytes;
    });
    return new NetworkUsage(inBytes, outBytes);
  }
}
