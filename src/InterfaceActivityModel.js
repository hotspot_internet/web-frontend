/* eslint-disable no-underscore-dangle */
import InterfaceActivity from './InterfaceActivity';

export default class InterfaceActivityModel {
  links: Object
  interfaceActivity: InterfaceActivity

  constructor(json: Object) {
    this.links = json._links;
    this.interfaceActivity = new InterfaceActivity(json);
  }

  getLinkHref(link: String): String {
    const linkObject = this.links[link];
    return linkObject ? this.links[link].href : undefined;
  }
}
