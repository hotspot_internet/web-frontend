/* eslint-disable no-underscore-dangle */
import { InterfaceReading } from './InterfaceReading';

export default class InterfaceReadingCollection {
  links: Object
  readings: InterfaceReading[]

  constructor(json: Object) {
    this.links = json._links;
    if (json._embedded) {
      this.readings = json
        ._embedded
        .interfaceReadingList
        .map((readingJson) => new InterfaceReading(readingJson));
    } else {
      this.readings = [];
    }
  }
}
