// eslint-disable-next-line object-curly-newline
const config = {};

config.restHttpPort = process.env.REACT_APP_REST_HTTP_PORT || 8080;
config.restHost = process.env.REACT_APP_REST_HOST || 'hotspot_rest_service_1';
config.restServer = `http://${config.restHost}:${config.restHttpPort}`;

config.dataLimitUrl = `${config.restServer}/datalimit/interface`;
config.interfaceUrl = `${config.restServer}/activity/interface`;
config.maxInterfaceIndex = 3;

module.exports = config;
