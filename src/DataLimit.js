const CyclePeriod = {
  MONTH: 'MONTH',
  DAY: 'DAY',
};

function getCyclePeriod(json: Object): String {
  /* eslint-disable-next-line */
  for (const property in CyclePeriod) {
    if (property === json.cyclePeriod) {
      return property;
    }
  }
  throw new Error('JSON does not contain cyclePeriod');
}

function getDifferenceDays(date1: Date, date2: Date): number {
  const diffTime = Math.abs(date2 - date1);
  return diffTime / (1000 * 60 * 60 * 24);
}

class DataLimit {
  dataLimitId: number
  interfaceIndex: number
  bytes: number
  startInstant: Date
  stopInstant: Date
  cyclePeriod: String

  constructor(json: Object) {
    this.dataLimitId = json.dataLimitId;
    this.interfaceIndex = json.interfaceIndex;
    this.bytes = json.bytes;
    this.startInstant = new Date(json.startInstant);
    this.stopInstant = json.stopInstant == null ? null : new Date(json.stopInstant);
    this.cyclePeriod = getCyclePeriod(json);
  }

  getAllowedBytes(date: Date): number {
    const cycleStartDate = this.getCycleStartDate(date);
    const daysInCycle = this.getDaysInCycle(date);
    const daysFromStart = getDifferenceDays(cycleStartDate, date);
    return this.bytes * (daysFromStart / daysInCycle);
  }

  getCycleStopDate(date: Date): Date {
    const cycleStartDate = this.getCycleStartDate(date);
    const cycleStopDate = new Date(cycleStartDate);
    const cycleStartMonth = cycleStartDate.getUTCMonth();
    if (cycleStartMonth === 11) {
      cycleStopDate.setUTCFullYear(cycleStartDate.getUTCFullYear() + 1);
      cycleStopDate.setUTCMonth(0);
    } else {
      cycleStopDate.setUTCMonth(cycleStartMonth + 1);
    }
    if (this.stopInstant !== null && this.stopInstant.getTime() < cycleStopDate.getTime()) {
      return this.stopInstant;
    }
    return cycleStopDate;
  }

  getDaysInCycle(date: Date): number {
    const cycleStartDate = this.getCycleStartDate(date);
    const cycleStopDate = this.getCycleStopDate(date);
    return getDifferenceDays(cycleStartDate, cycleStopDate);
  }

  getCycleStartDate(date: Date): Date {
    if (this.cyclePeriod !== CyclePeriod.MONTH) {
      throw new Error(`Unhandled period of '${this.cyclePeriod}'`);
    }

    const cycleStartDateSameMonth = new Date(this.startInstant);
    cycleStartDateSameMonth.setUTCMonth(date.getUTCMonth());
    cycleStartDateSameMonth.setUTCFullYear(date.getUTCFullYear());
    if (cycleStartDateSameMonth.getTime() <= date.getTime()) {
      return cycleStartDateSameMonth;
    }

    const cycleStartDate = new Date(cycleStartDateSameMonth);
    const cycleStartMonth = cycleStartDate.getUTCMonth();
    if (cycleStartMonth === 0) {
      cycleStartDate.setUTCFullYear(cycleStartDate.getUTCFullYear() - 1);
      cycleStartDate.setUTCMonth(11);
    } else {
      cycleStartDate.setUTCMonth(cycleStartMonth - 1);
    }
    return cycleStartDate;
  }
}

export default DataLimit;
