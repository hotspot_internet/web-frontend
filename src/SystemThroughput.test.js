import { AdminStatus, InterfaceReading, OperStatus } from './InterfaceReading';
import InterfaceThroughput from './InterfaceThroughput';
import InterfaceUsageReport from './InterfaceUsageReport';
import { State } from './Throughput';
import SystemThroughput from './SystemThroughput';

function createReading(interfaceIndex: number, adminStatus, operStatus): InterfaceReading {
  const json = {
    instant: '2020-01-02T00:00:00Z',
    inBytes: 1,
    outBytes: 2,
    interfaceIndex,
    adminStatus,
    operStatus,
  };
  return new InterfaceReading(json);
}

function createUsageReport(
  interfaceIndex: number,
  usedPercent: number,
  dayOfMonth: number,
): InterfaceUsageReport {
  const json = {
    instant: new Date(2020, 0, dayOfMonth),
    interfaceIndex,
    usedPercent,
  };
  return new InterfaceUsageReport(json);
}

test('test 1 of 2 interfaces on', () => {
  const systemThroughput = new SystemThroughput(
    [
      new InterfaceThroughput(
        createReading(1, AdminStatus.DOWN, OperStatus.DOWN),
        createUsageReport(1, 100.0, 1),
      ),
      new InterfaceThroughput(
        createReading(2, AdminStatus.UP, OperStatus.UP),
        createUsageReport(2, 90.0, 1),
      ),
    ],
  );

  expect(systemThroughput.getDescription()).toBe('1/2');
  expect(systemThroughput.getState()).toBe(State.OK);
});

test('test throttled', () => {
  const systemThroughput = new SystemThroughput(
    [
      new InterfaceThroughput(
        createReading(1, AdminStatus.UP, OperStatus.UP),
        createUsageReport(1, 100.0, 1),
      ),
      new InterfaceThroughput(
        createReading(2, AdminStatus.UP, OperStatus.UP),
        createUsageReport(2, 100.0, 1),
      ),
    ],
  );

  expect(systemThroughput.getDescription()).toBe(State.THROTTLED);
  expect(systemThroughput.getState()).toBe(State.THROTTLED);
});

test('test error', () => {
  const systemThroughput = new SystemThroughput(
    [
      new InterfaceThroughput(
        createReading(1, AdminStatus.UP, OperStatus.DOWN),
        createUsageReport(1, 90.0, 1),
      ),
    ],
  );

  expect(systemThroughput.getDescription()).toBe('1 interface(s) in error');
  expect(systemThroughput.getState()).toBe(State.ERROR);
});
