const State = Object.freeze({
  OK: 'OK',
  THROTTLED: 'THROTTLED',
  ERROR: 'ERROR',
  OFF: 'OFF',
  UNKNOWN: 'UNKNOWN',
});

export interface Throughput {
  getState(): String;
  getDescription(): String;
}

export { State };
