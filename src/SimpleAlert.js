import React from 'react';
import { makeStyles, createStyles, Theme } from '@material-ui/core/styles';
import Alert from '@material-ui/lab/Alert';
import PropTypes from 'prop-types';

const useStyles = makeStyles((theme: Theme) => createStyles({
  root: {
    width: '100%',
    '& > * + *': {
      marginTop: theme.spacing(2),
    },
  },
}));

export default function SimpleAlert(props) {
  const classes = useStyles();
  const { alertMessage, onClose } = props;

  return (
    <div className={classes.root}>
      <Alert severity="error" onClose={onClose}>{alertMessage}</Alert>
    </div>
  );
}

SimpleAlert.propTypes = {
  alertMessage: PropTypes.string.isRequired,
  onClose: PropTypes.func,
};

SimpleAlert.defaultProps = {
  onClose: null,
};
