import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import { Link } from '@material-ui/core';
import config from './config';

const parentDesignUrl = 'https://gitlab.com/hotspot_internet/parent/-/jobs/artifacts/master/file/build/docs/design.html?job=build';
const restApiUrl = `${config.restServer}/rest_api.html`;
const restApiSecurityUrl = `${config.restServer}/security.html`;

const useStyles = makeStyles((theme) => ({
  root: {
    '& > * + *': {
      marginLeft: theme.spacing(2),
    },
  },
}));

export default function AboutTab() {
  const classes = useStyles();

  return (
    <Typography className={classes.root} component="span">
      <Typography variant="h1">
        Hotspot Internet Project
      </Typography>
      <br />
      <Typography variant="h2">
        <Link href={parentDesignUrl} target="_blank" rel="noopener">
          High-Level Purpose and Design
        </Link>
      </Typography>
      <br />
      <Typography variant="h2">
        REST API
      </Typography>
      <Typography variant="h3">
        &emsp;&emsp;&emsp;o&nbsp;Used by website to access the backend
      </Typography>
      <Typography variant="h3">
        &emsp;&emsp;&emsp;o&nbsp;
        <Link href={restApiUrl}>
          Overview
        </Link>
      </Typography>
      <Typography variant="h3">
        &emsp;&emsp;&emsp;o&nbsp;
        <Link href={restApiSecurityUrl}>
          REST API Security
        </Link>
      </Typography>
    </Typography>
  );
}
