import React from 'react';
import {
  Chart,
  Series,
  ArgumentAxis,
  CommonSeriesSettings,
  Export,
  Legend,
  Margin,
  Title,
  TickInterval,
  Subtitle,
  Tooltip,
  Grid,
  ValueAxis,
} from 'devextreme-react/chart';
import NetworkUsage from './NetworkUsage';
import UsageChart from './UsageChart';

function toGbTwoDecimals(decimal: number): number {
  return Math.round(decimal / 1.0e7) / 100;
}

function getDifferenceDays(date1: Date, date2: Date): number {
  const diffTime = Math.abs(date2 - date1);
  return diffTime / (1000 * 60 * 60 * 24);
}

interface FieldInfo {
  value: String;
  name: String;
  color: String;
}

const fieldInfos = [
  {
    value: 'actualGb',
    name: 'Actual',
    color: 'Blue',
  },
  {
    value: 'allowedGb',
    name: 'Allowed',
    color: 'Red',
  },
];

function createDataSourceItem(aggregateBytes: number, allowedBytes: number, date: Date): Object {
  return ({
    actualGb: toGbTwoDecimals(aggregateBytes),
    allowedGb: toGbTwoDecimals(allowedBytes),
    date,
  });
}

export default class CumulativeChart extends UsageChart {
  static getCumulativeUsages(networkUsages: NetworkUsage[]): NetworkUsage[] {
    const cumulativeUsages = [];
    let lastSummedUsage = new NetworkUsage(0, 0);
    for (let index = 0; index < networkUsages.length; index += 1) {
      const summedUsage = NetworkUsage.sumNetworkUsages([lastSummedUsage, networkUsages[index]]);
      cumulativeUsages.push(summedUsage);
      lastSummedUsage = summedUsage;
    }
    return cumulativeUsages;
  }

  getCycleDescription() {
    const { dataLimits } = this.state;
    const activityArrays = this.getActivityArrays();
    if (activityArrays.length === 0) {
      return '';
    }
    const firstActivity = activityArrays[0][0];
    const cycleStartLocalString = dataLimits[0]
      .getCycleStartDate(firstActivity.start)
      .toLocaleDateString();
    const cycleStopLocalString = dataLimits[0]
      .getCycleStopDate(firstActivity.start)
      .toLocaleDateString();
    return `Cycle: ${cycleStartLocalString}-${cycleStopLocalString}`;
  }

  render() {
    const { dataLimits } = this.state;
    const activityArrays = this.getActivityArrays();
    const networkUsages = CumulativeChart.getCumulativeUsages(
      this.getNetworkUsages(activityArrays),
    );

    let dataSource = [];
    if (activityArrays.length !== 0) {
      dataSource.push(createDataSourceItem(0, 0, activityArrays[0][0].start));
    }
    const totalBytesInCycle = dataLimits.reduce(
      (previous, dataLimit) => previous + dataLimit.bytes,
      0,
    );
    const daysInCycle = activityArrays.length !== 0
      ? dataLimits[0].getDaysInCycle(activityArrays[0][0].start)
      : 0;
    dataSource = dataSource.concat(networkUsages
      .map((networkUsage, index) => {
        const daysFromStart = getDifferenceDays(
          activityArrays[0][0].start,
          activityArrays[0][index].stop,
        );
        return createDataSourceItem(
          networkUsage.aggregateBytes(),
          totalBytesInCycle * (daysFromStart / daysInCycle),
          activityArrays[0][index].stop,
        );
      }));

    return (
      <>
        {this.renderProgressBackdrop()}
        <Chart
          dataSource={dataSource}
        >
          <CommonSeriesSettings
            argumentField="date"
            type="line"
          />
          {fieldInfos.map((fieldInfo: FieldInfo) => (
            <Series
              color={fieldInfo.color}
              key={fieldInfo.value}
              valueField={fieldInfo.value}
              name={fieldInfo.name}
            />
          ))}
          <Margin bottom={20} />
          <ArgumentAxis
            valueMarginsEnabled="false"
            discreteAxisDivisionMode="crossLabels"
          >
            <TickInterval days={1} />
            <Grid visible="true" />
          </ArgumentAxis>
          <ValueAxis
            title="GB"
            name="GB"
            position="left"
            tickInterval={10}
            showZero="true"
            valueMarginsEnabled="false"
          />
          <Legend
            show
            verticalAlignment="bottom"
            horizontalAlignment="center"
            itemTextPosition="bottom"
          />
          <Export enabled="true" />
          <Title text="Cumulative Data Usage">
            <Subtitle text={this.getCycleDescription()} />
          </Title>
          <Tooltip enabled="true" />
        </Chart>
        {this.renderError()}
        {this.renderForwardBackButtons()}
      </>
    );
  }
}
