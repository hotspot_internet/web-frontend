import { Throughput, State } from './Throughput';
import InterfaceThroughput from './InterfaceThroughput';

export default class SystemThroughput implements Throughput {
  interfaceThroughputs: InterfaceThroughput[]
  state: String
  description: String

  constructor(interfaceThroughputs: InterfaceThroughput[]) {
    this.interfaceThroughputs = interfaceThroughputs;
    const numThroughputs = interfaceThroughputs.length;
    const numRunning = interfaceThroughputs
      .filter((interfaceThroughput) => interfaceThroughput.isRunning())
      .length;
    const numErrors = interfaceThroughputs
      .filter((interfaceThroughput) => interfaceThroughput.isError())
      .length;
    const numExceeded = interfaceThroughputs
      .filter((interfaceThroughput) => interfaceThroughput.isLimitExceeded())
      .length;

    if (numThroughputs === 0) {
      this.setState(State.UNKNOWN);
    } else if (numErrors > 0) {
      this.setStateAndDescription(State.ERROR, `${numErrors} interface(s) in error`);
    } else if (numRunning === 0) {
      this.setStateAndDescription(State.ERROR, 'None on');
    } else if (numExceeded === numThroughputs) {
      this.setState(State.THROTTLED);
    } else if (numExceeded > 0) {
      this.setStateAndDescription(State.OK, `${numThroughputs - numExceeded}/${numThroughputs}`);
    } else {
      this.setState(State.OK);
    }
  }

  setState(state: String) {
    this.setStateAndDescription(state, state);
  }

  setStateAndDescription(state: String, description: String) {
    this.state = state;
    this.description = description;
  }

  getState() {
    return this.state;
  }

  getDescription() {
    return this.description;
  }
}
