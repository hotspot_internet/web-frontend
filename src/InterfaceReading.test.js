import { InterfaceReading } from './InterfaceReading';

test('constructs from JSON', () => {
  const responseJson = {
    instant: '1970-01-01T00:01:05Z',
    interfaceIndex: 0,
    inBytes: 1,
    outBytes: 2,
    adminStatus: 'UP',
    operStatus: 'DOWN',
  };
  const interfaceReading = new InterfaceReading(responseJson);
  expect(interfaceReading.instant.getTime()).toBe(65000);
  expect(interfaceReading.interfaceIndex).toBe(0);
  expect(interfaceReading.inBytes).toBe(1);
  expect(interfaceReading.outBytes).toBe(2);
  expect(interfaceReading.aggregateBytes()).toBe(3);
  expect(interfaceReading.adminStatus).toBe('UP');
  expect(interfaceReading.operStatus).toBe('DOWN');
});
