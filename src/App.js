import React from 'react';
import 'devextreme/dist/css/dx.common.css';
import 'devextreme/dist/css/dx.light.css';
import { AppBarWithTabs, TabInfo } from './AppBarWithTabs';
import AboutTab from './AboutTab';
import UsageTablePanel from './UsageTablePanel';
import CumulativeChart from './CumulativeChart';
import IntervalChart from './IntervalChart';

export default function App() {
  return (
    <AppBarWithTabs tabs={[
      new TabInfo('About', AboutTab),
      new TabInfo('Usage Table', UsageTablePanel),
      new TabInfo('Cumulative Chart', CumulativeChart),
      new TabInfo('Interval Chart', IntervalChart),
    ]}
    />
  );
}
