import React from 'react';
import {
  Chart,
  Series,
  ArgumentAxis,
  CommonSeriesSettings,
  Export,
  Legend,
  Title,
  Grid,
  ValueAxis, Subtitle,
} from 'devextreme-react/chart';
import ZoomOutIcon from '@material-ui/icons/ZoomOut';
import { Button } from '@material-ui/core';
import UsageChart from './UsageChart';
import { InterfaceActivityCollection, Link } from './InterfaceActivityCollection';
import InterfaceActivityModel from './InterfaceActivityModel';
import NetworkUsage from './NetworkUsage';

function toGbTwoDecimals(decimal: number): number {
  return Math.round(decimal / 1.0e7) / 100;
}

function getDifferenceDays(date1: Date, date2: Date): number {
  const diffTime = Math.abs(date2 - date1);
  return diffTime / (1000 * 60 * 60 * 24);
}

interface FieldInfo {
  value: String;
  name: String;
  color: String;
}

const fieldInfos = [
  {
    value: 'inGb',
    name: 'In',
    color: 'Blue',
  },
  {
    value: 'outGb',
    name: 'Out',
    color: 'Red',
  },
];

export default class IntervalChart extends UsageChart {
  constructor(props) {
    super(props);
    this.onPointClick = this.onPointClick.bind(this);
  }

  render() {
    const activityArrays = this.getActivityArrays();
    const networkUsages = this.getNetworkUsages();
    const totalUsage = NetworkUsage.sumNetworkUsages(networkUsages);

    const dataSource = networkUsages.map((networkUsage, index) => ({
      inGb: networkUsage.inBytes / 1.0e9,
      outGb: networkUsage.outBytes / 1.0e9,
      date: activityArrays[0][index].start,
    }));

    return (
      <>
        {this.renderProgressBackdrop()}
        <Button
          variant="contained"
          color="primary"
          endIcon={<ZoomOutIcon />}
          disabled={!this.hasIntervalIn()}
          onClick={() => this.handleLinkName(Link.INTERVAL_IN)}
        >
          Zoom Out
        </Button>
        <Chart
          id="chart"
          dataSource={dataSource}
          onPointClick={this.onPointClick}
          className={this.hasIntervalContains() ? 'pointer-on-bars' : ''}
        >
          <CommonSeriesSettings argumentField="date" type="stackedBar" />
          {fieldInfos.map((fieldInfo: FieldInfo) => (
            <Series
              color={fieldInfo.color}
              key={fieldInfo.value}
              valueField={fieldInfo.value}
              name={fieldInfo.name}
            />
          ))}
          <ArgumentAxis
            valueMarginsEnabled="false"
            discreteAxisDivisionMode="crossLabels"
          >
            <Grid visible="true" />
          </ArgumentAxis>
          <ValueAxis position="right">
            <Title text="GB" />
          </ValueAxis>
          <Legend
            verticalAlignment="bottom"
            horizontalAlignment="center"
            itemTextPosition="top"
          />
          <Export enabled />
          <Title text="Interval Data Usage">
            <Subtitle text={
              `Total Usage: ${toGbTwoDecimals(totalUsage.aggregateBytes())} GB
              ${this.getRangeDescription()}`
            }
            />
          </Title>
        </Chart>
        {this.renderError()}
        {this.renderForwardBackButtons()}
      </>
    );
  }

  hasIntervalIn(): boolean {
    const { activityCollections } = this.state;
    return activityCollections.length === 0
      ? false
      : activityCollections[0].getLinkHref(Link.INTERVAL_IN) !== undefined;
  }

  hasIntervalContains(): boolean {
    const { activityCollections } = this.state;
    return activityCollections.length === 0
      ? false
      : activityCollections[0].activityModels[0].getLinkHref(Link.INTERVAL_CONTAINS) !== undefined;
  }

  getRangeDescription() {
    const activityArrays = this.getActivityArrays();
    if (activityArrays.length === 0) {
      return '';
    }
    const { start } = activityArrays[0][0];
    const { stop } = activityArrays[0][activityArrays[0].length - 1];
    const diffDays = getDifferenceDays(start, stop);
    return diffDays <= 1
      ? `${start.toLocaleString()}  -  ${stop.toLocaleString()}`
      : `${start.toLocaleDateString()}  -  ${stop.toLocaleDateString()}`;
  }

  onPointClick(event) {
    if (!this.hasIntervalContains()) {
      this.setState({
        error: 'No further breakdown available',
      });
      return;
    }

    const { activityCollections } = this.state;
    const clickedDate = event.target.originalArgument;
    const index = activityCollections[0]
      .activityModels
      .map((model: InterfaceActivityModel) => model.interfaceActivity.start)
      .findIndex((startDate) => startDate.getTime() === clickedDate.getTime());
    this.updateActivities(activityCollections
      .map((collection: InterfaceActivityCollection) => collection
        .activityModels[index]
        .getLinkHref(Link.INTERVAL_CONTAINS)));
  }
}
