import NetworkUsage from './NetworkUsage';

const AdminStatus = {
  UP: 'UP',
  DOWN: 'DOWN',
  TESTING: 'TESTING',
};
const OperStatus = {
  UP: 'UP',
  DOWN: 'DOWN',
  TESTING: 'TESTING',
  UNKNOWN: 'UNKNOWN',
  DORMANT: 'DORMANT',
  NOT_PRESENT: 'NOT_PRESENT',
  LOWER_LAYER_DOWN: 'LOWER_LAYER_DOWN',
};

function getAdminStatus(json: Object) {
  /* eslint-disable-next-line */
  for (const property in AdminStatus) {
    if (property === json.adminStatus) {
      return property;
    }
  }
  throw new Error('JSON does not contain adminStatus');
}

function getOperStatus(json: Object) {
  /* eslint-disable-next-line */
  for (const property in OperStatus) {
    if (property === json.operStatus) {
      return property;
    }
  }
  throw new Error('JSON does not contain operStatus');
}

class InterfaceReading extends NetworkUsage {
  instant: Date
  interfaceIndex: number
  adminStatus
  operStatus

  constructor(json: Object) {
    super(json.inBytes, json.outBytes);
    this.instant = new Date(json.instant);
    this.interfaceIndex = json.interfaceIndex;
    this.adminStatus = getAdminStatus(json);
    this.operStatus = getOperStatus(json);
  }
}

export { InterfaceReading, AdminStatus, OperStatus };
