import React from 'react';
import { render, screen, waitFor } from '@testing-library/react';
import axios from 'axios';
import UsageTablePanel from './UsageTablePanel';

jest.mock('axios');

function createReadingJson(interfaceIndex: number): Object {
  return {
    data: {
      _embedded: {
        interfaceReadingList: [
          {
            instant: '1970-01-01T00:01:05Z',
            interfaceIndex,
            inBytes: 1.11e9,
            outBytes: 2.11e9,
            adminStatus: 'UP',
            operStatus: 'DOWN',
          },
        ],
      },
    },
  };
}

function createUsageReportJson(interfaceIndex: number): Object {
  return {
    data: [
      {
        instant: new Date().toString(),
        interfaceIndex,
        usedPercent: 90.0,
      },
    ],
  };
}

function mockAxios(reading1, reading2, reading3, report1, report2, report3) {
  axios.get.mockImplementation((url: String) => {
    if (url.includes('reading/interface/1')) {
      return Promise.resolve(reading1);
    }
    if (url.includes('reading/interface/2')) {
      return Promise.resolve(reading2);
    }
    if (url.includes('reading/interface/3')) {
      return Promise.resolve(reading3);
    }
    if (url.includes('usagereport/interface/1')) {
      return Promise.resolve(report1);
    }
    if (url.includes('usagereport/interface/2')) {
      return Promise.resolve(report2);
    }
    if (url.includes('usagereport/interface/3')) {
      return Promise.resolve(report3);
    }
    throw new Error('no value mocked for provided URL');
  });
}

test('renders readings', async () => {
  mockAxios(
    createReadingJson(1), createReadingJson(2), createReadingJson(3),
    createUsageReportJson(1), createUsageReportJson(2), createUsageReportJson(3),
  );
  render(<UsageTablePanel />);
  await waitFor(() => expect(screen.getAllByText(/1.11/i)).toHaveLength(3));
  await waitFor(() => expect(screen.getAllByText(/2.11/i)).toHaveLength(3));
  await waitFor(() => expect(screen.getAllByText(/3.22/i)).toHaveLength(3));
  await waitFor(() => expect(screen.getByText(/3.33/i)).toBeInTheDocument());
  await waitFor(() => expect(screen.getByText(/6.33/i)).toBeInTheDocument());
  await waitFor(() => expect(screen.getByText(/9.66/i)).toBeInTheDocument());
});

test('renders error when no readings', async () => {
  const response = {
    data: {
      _embedded: {
        interfaceReadingList: [],
      },
    },
  };
  axios.get.mockImplementation(() => Promise.resolve(response));
  render(<UsageTablePanel />);
  await waitFor(() => expect(screen.getByText(/No readings available/i)).toBeInTheDocument());
});

test('renders REST error', async () => {
  const reason = {
    response: {
      data: 'some error',
    },
  };
  axios.get.mockImplementation(() => Promise.reject(reason));
  render(<UsageTablePanel />);
  await waitFor(() => expect(screen.getByText(/some error/i)).toBeInTheDocument());
});
