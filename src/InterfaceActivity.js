import NetworkUsage from './NetworkUsage';

export default class InterfaceActivity extends NetworkUsage {
  start: Date
  stop: Date
  interfaceIndex: number

  constructor(json: Object) {
    super(json.inBytes, json.outBytes);
    this.start = new Date(json.startInstant);
    this.stop = new Date(json.stopInstant);
    this.interfaceIndex = json.interfaceIndex;
  }
}
