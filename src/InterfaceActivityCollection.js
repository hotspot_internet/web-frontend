/* eslint-disable no-underscore-dangle */
import InterfaceActivityModel from './InterfaceActivityModel';

const Link = Object.freeze({
  INTERVAL_CONTAINS: 'intervalContains',
  INTERVAL_IN: 'intervalIn',
  SELF: 'self',
  NEXT: 'next',
  PREVIOUS: 'prev',
});

class InterfaceActivityCollection {
  links: Object
  activityModels: InterfaceActivityModel[]

  constructor(json: Object) {
    this.links = json._links;
    if (json._embedded) {
      this.activityModels = json
        ._embedded
        .interfaceActivityList
        .map((activityJson) => new InterfaceActivityModel(activityJson));
    } else {
      this.activityModels = [];
    }
  }

  getLinkHref(link: String): String {
    const linkObject = this.links[link];
    return linkObject ? this.links[link].href : undefined;
  }
}

export { InterfaceActivityCollection, Link };
