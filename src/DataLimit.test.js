import DataLimit from './DataLimit';

const dataLimitJson = {
  dataLimitId: 1,
  interfaceIndex: 2,
  bytes: 29,
  startInstant: '2020-11-19Z',
  stopInstant: null,
  cyclePeriod: 'MONTH',
};
const dataLimit = new DataLimit(dataLimitJson);

test('constructs from JSON', () => {
  expect(dataLimit.dataLimitId).toBe(1);
  expect(dataLimit.interfaceIndex).toBe(2);
  expect(dataLimit.bytes).toBe(29);
  expect(dataLimit.stopInstant).toBe(null);
  expect(dataLimit.cyclePeriod).toBe('MONTH');
});

test('gets cycle start in previous year', () => {
  const actualCycleStart = dataLimit.getCycleStartDate(new Date('2021-1-18Z'));
  const expectedCycleStart = new Date('2020-12-19Z');
  expect(actualCycleStart).toStrictEqual(expectedCycleStart);
});

test('gets cycle start in previous month', () => {
  const actualCycleStart = dataLimit.getCycleStartDate(new Date('2021-3-3Z'));
  const expectedCycleStart = new Date('2021-2-19Z');
  expect(actualCycleStart).toStrictEqual(expectedCycleStart);
});

test('gets cycle stop current month', () => {
  const actualCycleStop = dataLimit.getCycleStopDate(new Date('2021-3-17Z'));
  const expectedCycleStop = new Date('2021-3-19Z');
  expect(actualCycleStop).toStrictEqual(expectedCycleStop);
});

test('gets cycle stop in next month', () => {
  const actualCycleStop = dataLimit.getCycleStopDate(new Date('2021-3-25Z'));
  const expectedCycleStop = new Date('2021-4-19Z');
  expect(actualCycleStop).toStrictEqual(expectedCycleStop);
});

test('gets cycle stop in next year', () => {
  const actualCycleStop = dataLimit.getCycleStopDate(new Date('2022-12-25Z'));
  const expectedCycleStop = new Date('2023-1-19Z');
  expect(actualCycleStop).toStrictEqual(expectedCycleStop);
});

test('gets days in cycle', () => {
  const daysInCycle = dataLimit.getDaysInCycle(new Date('2020-11-24Z'));
  expect(daysInCycle).toStrictEqual(30);
});

test('gets allowed bytes halfway', () => {
  // 15 days into a 30-day cycle (i.e. halfway)
  const allowedBytes = dataLimit.getAllowedBytes(new Date('2020-12-4Z'));
  expect(allowedBytes).toStrictEqual(14.5);
});

test('gets allowed bytes at cycle start', () => {
  // 15 days into a 30-day cycle (i.e. halfway)
  const allowedBytes = dataLimit.getAllowedBytes(new Date('2019-11-19Z'));
  expect(allowedBytes).toStrictEqual(0);
});
