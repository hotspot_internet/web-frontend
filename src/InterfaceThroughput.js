import { AdminStatus, InterfaceReading, OperStatus } from './InterfaceReading';
import InterfaceUsageReport from './InterfaceUsageReport';
import { Throughput, State } from './Throughput';

export default class InterfaceThroughput implements Throughput {
  interfaceReading: InterfaceReading
  usageReport: InterfaceUsageReport

  constructor(interfaceReading: InterfaceReading, usageReport: InterfaceUsageReport) {
    this.interfaceReading = interfaceReading;
    this.usageReport = usageReport;
  }

  getState() {
    switch (this.interfaceReading.adminStatus) {
      case AdminStatus.DOWN:
        return State.OFF;
      case AdminStatus.UP:
        if (this.interfaceReading.operStatus === OperStatus.UP) {
          return this.isLimitExceeded() ? State.THROTTLED : State.OK;
        }
        return State.ERROR;
      default:
        return State.ERROR;
    }
  }

  static interfaceSorter(
    throughput1: InterfaceThroughput,
    throughput2: InterfaceThroughput,
  ): number {
    return throughput1.interfaceReading.interfaceIndex
      - throughput2.interfaceReading.interfaceIndex;
  }

  getDescription() {
    if (this.getState() === State.ERROR) {
      return this.interfaceReading.adminStatus !== AdminStatus.UP
        ? this.interfaceReading.adminStatus
        : this.interfaceReading.operStatus;
    }
    return this.getState();
  }

  isLimitExceeded(): boolean {
    return this.usageReport === undefined
      ? false
      : this.interfaceReading.instant.getTime() > this.usageReport.instant.getTime()
        && !this.usageReport.isDataLeft();
  }

  isRunning(): boolean {
    return this.interfaceReading.operStatus === OperStatus.UP;
  }

  isError(): boolean {
    return this.getState() === State.ERROR;
  }
}
