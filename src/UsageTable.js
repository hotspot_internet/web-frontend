import React from 'react';
import { withStyles, Theme, makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import PropTypes from 'prop-types';
import { InterfaceReading } from './InterfaceReading';
import NetworkUsage from './NetworkUsage';
import { Throughput, State } from './Throughput';
import SystemThroughput from './SystemThroughput';
import InterfaceThroughput from './InterfaceThroughput';
import InterfaceUsageReport from './InterfaceUsageReport';

const StyledTableCell = withStyles((theme) => ({
  head: {
    backgroundColor: theme.palette.common.black,
    color: theme.palette.common.white,
  },
  body: {
    fontSize: 14,
  },
}))(TableCell);

const StyledTableRow = withStyles((theme: Theme) => ({
  root: {
    '&:nth-of-type(odd)': {
      backgroundColor: theme.palette.action.hover,
    },
  },
}))(TableRow);

const useStyles = makeStyles({
  table: {
    minWidth: 300,
    maxWidth: 700,
  },
});

function toGbTwoDecimals(decimal: number): number {
  return (Math.round(decimal / 1e7) / 100).toFixed(2);
}

const getStateColor = (state: State): String => {
  switch (state) {
    case State.ERROR:
      return 'red';
    case State.OK:
      return 'green';
    case State.OFF:
    case State.UNKNOWN:
      return 'grey';
    case State.THROTTLED:
    default:
      return 'yellow';
  }
};

function getStyle(throughput: Throughput): Object {
  return ({
    backgroundColor: getStateColor(throughput.getState()),
    color: getStateColor(throughput.getState()) === 'yellow' ? 'black' : 'white',
  });
}

function getUsageReportFunction(usageReports: InterfaceUsageReport[]) {
  return (reading: InterfaceReading) => usageReports
    .filter((report) => reading.interfaceIndex === report.interfaceIndex)[0];
}

function UsageTable(props) {
  const classes = useStyles();
  const { readings, totalUsage, usageReports } = props;
  const getUsageReportFromReading = getUsageReportFunction(usageReports);
  const throughputs = readings
    .map((reading) => new InterfaceThroughput(reading, getUsageReportFromReading(reading)))
    .sort(InterfaceThroughput.interfaceSorter);
  const systemThroughput = new SystemThroughput(throughputs);
  return (
    <TableContainer component={Paper}>
      <Table className={classes.table} aria-label="customized table">
        <TableHead>
          <TableRow>
            <StyledTableCell>Interface</StyledTableCell>
            <StyledTableCell align="right">In (GB)</StyledTableCell>
            <StyledTableCell align="right">Out (GB)</StyledTableCell>
            <StyledTableCell align="right">Total (GB)</StyledTableCell>
            <StyledTableCell align="right">Status</StyledTableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {throughputs.map((throughput: InterfaceThroughput) => {
            const reading = throughput.interfaceReading;
            return (
              <StyledTableRow key={reading.interfaceIndex}>
                <StyledTableCell component="th" scope="row">
                  {reading.interfaceIndex}
                </StyledTableCell>
                <StyledTableCell align="right">{toGbTwoDecimals(reading.inBytes)}</StyledTableCell>
                <StyledTableCell align="right">{toGbTwoDecimals(reading.outBytes)}</StyledTableCell>
                <StyledTableCell align="right">{toGbTwoDecimals(reading.aggregateBytes())}</StyledTableCell>
                <StyledTableCell
                  align="right"
                  style={getStyle(throughput)}
                >
                  {throughput.getDescription()}
                </StyledTableCell>
              </StyledTableRow>
            );
          })}
          <StyledTableRow key="total">
            <StyledTableCell component="th" scope="row">
              Total
            </StyledTableCell>
            <StyledTableCell align="right">{toGbTwoDecimals(totalUsage.inBytes)}</StyledTableCell>
            <StyledTableCell align="right">{toGbTwoDecimals(totalUsage.outBytes)}</StyledTableCell>
            <StyledTableCell align="right">{toGbTwoDecimals(totalUsage.aggregateBytes())}</StyledTableCell>
            <StyledTableCell
              align="right"
              style={getStyle(systemThroughput)}
            >
              {systemThroughput.getDescription()}
            </StyledTableCell>
          </StyledTableRow>
        </TableBody>
      </Table>
    </TableContainer>
  );
}

UsageTable.propTypes = {
  totalUsage: PropTypes.instanceOf(NetworkUsage),
  readings: PropTypes.arrayOf(InterfaceReading),
  usageReports: PropTypes.arrayOf(InterfaceUsageReport),
};

UsageTable.defaultProps = {
  totalUsage: null,
  readings: [],
  usageReports: [],
};

export default UsageTable;
