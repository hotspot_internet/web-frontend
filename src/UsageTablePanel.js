import React from 'react';
import './App.css';
import DateFnsUtils from '@date-io/date-fns';
import {
  DateTimePicker,
  MuiPickersUtilsProvider,
} from '@material-ui/pickers';
import axios from 'axios';
import config from './config';
import SimpleAlert from './SimpleAlert';
import UsageTable from './UsageTable';
import { InterfaceReading } from './InterfaceReading';
import InterfaceUsageReport from './InterfaceUsageReport';
import NetworkUsage from './NetworkUsage';
import InterfaceReadingCollection from './InterfaceReadingCollection';
import ProgressBackdrop from './ProgressBackdrop';

const interfaceUrl = `${config.restServer}/reading/interface`;
const maxInterfaceIndex = 3;
function createUrl(interfaceIndex: number, start: Date): String {
  const parameters = start == null ? '' : `?start=${start.toISOString()}`;
  return `${interfaceUrl}/${interfaceIndex}${parameters}`;
}

const usageReportUrl = `${config.restServer}/notifications/usagereport/interface`;
function createUsageReportUrl(interfaceIndex: number, start: Date): String {
  const parameters = start == null ? '' : `?instant=${start.toISOString()}`;
  return `${usageReportUrl}/${interfaceIndex}${parameters}`;
}

function getErrorMessage(requestError: Object): String {
  if (requestError.response && requestError.response.data) {
    return requestError.response.data;
  }
  return requestError.message;
}

export default class UsageTablePanel extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      isRequestPending: false,
      readings: [],
      usageReports: [],
      selectedDate: null,
    };
    this.handleDateChange = this.handleDateChange.bind(this);
  }

  componentDidMount() {
    this.updateReadingsAndReports();
  }

  // eslint-disable-next-line no-unused-vars
  componentDidUpdate(prevProps, prevState, snapshot) {
    const { selectedDate, isRequestPending } = this.state;
    if (prevState.selectedDate !== null && selectedDate !== null) {
      const differenceMillis = Math.abs(prevState.selectedDate.getTime() - selectedDate.getTime());
      if (!isRequestPending && differenceMillis / 1e3 > 60) {
        this.updateReadingsAndReports();
      }
    }
  }

  handleDateChange(selectedDate: Date) {
    this.setState({
      error: null,
      selectedDate,
    });
  }

  updateReadingsAndReports() {
    this.setState({
      readings: [],
      usageReports: [],
      isRequestPending: true,
    });
    const promises = [];
    for (let index = 0; index < maxInterfaceIndex; index += 1) {
      promises.push(this.updateReading(index));
      promises.push(this.updateUsageReport(index));
    }
    Promise
      .all(promises)
      // eslint-disable-next-line no-unused-vars
      .then((completedPromises) => {
        this.setState({
          isRequestPending: false,
        });
      });
  }

  isRequestPending(): boolean {
    const { isRequestPending } = this.state;
    return isRequestPending;
  }

  updateReading(interfaceIndex: number): Promise {
    const { selectedDate } = this.state;
    const url = createUrl(interfaceIndex + 1, selectedDate);
    return axios.get(url)
      .then((response) => {
        // handle success
        const { readings } = this.state;
        const interfaceReadingList = new InterfaceReadingCollection(response.data).readings;
        if (interfaceReadingList.length > 0) {
          const interfaceReading = new InterfaceReading(interfaceReadingList[0]);
          readings.push(interfaceReading);
          this.setState({
            readings,
            selectedDate: selectedDate == null ? interfaceReading.instant : selectedDate,
            error: null,
          });
        } else {
          this.setState({
            error: 'No readings available for data limit',
            readings: [],
          });
        }
      })
      .catch((requestError) => {
        this.setState({
          error: getErrorMessage(requestError),
          readings: [],
        });
      });
  }

  updateUsageReport(interfaceIndex: number): Promise {
    const { selectedDate } = this.state;
    const url = createUsageReportUrl(interfaceIndex + 1, selectedDate);
    return axios.get(url)
      .then((response) => {
        // handle success
        const { usageReports } = this.state;
        if (response.data.length > 0) {
          const lastUsageReport = response.data
            .map((usageReportJson) => new InterfaceUsageReport(usageReportJson))
            .sort(InterfaceUsageReport.chronologicalSorter)
            .reverse()[0];
          usageReports.push(lastUsageReport);
          this.setState({
            usageReports,
          });
        }
      })
      .catch((requestError) => {
        this.setState({
          error: getErrorMessage(requestError),
        });
      });
  }

  render() {
    const { error, readings, usageReports, selectedDate } = this.state;
    const totalUsage = NetworkUsage.sumNetworkUsages(readings);
    return (
      <div>
        <ProgressBackdrop open={this.isRequestPending()} />
        {error == null
          ? <UsageTable readings={readings} totalUsage={totalUsage} usageReports={usageReports} />
          : <SimpleAlert alertMessage={error} />}
        <MuiPickersUtilsProvider utils={DateFnsUtils}>
          <DateTimePicker
            ampm={false}
            value={selectedDate}
            onChange={this.handleDateChange}
          />
        </MuiPickersUtilsProvider>
        <div>
          Year:
          {selectedDate == null ? 'loading...' : selectedDate.getFullYear()}
        </div>
      </div>
    );
  }
}
