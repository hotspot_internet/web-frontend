import { AdminStatus, InterfaceReading, OperStatus } from './InterfaceReading';
import InterfaceThroughput from './InterfaceThroughput';
import InterfaceUsageReport from './InterfaceUsageReport';
import { State } from './Throughput';

function createReading(adminStatus, operStatus): InterfaceReading {
  const json = {
    instant: '2020-01-02T00:01:05Z',
    interfaceIndex: 1,
    inBytes: 1,
    outBytes: 2,
    adminStatus,
    operStatus,
  };
  return new InterfaceReading(json);
}

function createUsageReport(usedPercent: number, dayOfMonth: number): InterfaceUsageReport {
  const json = {
    instant: new Date(2020, 0, dayOfMonth),
    interfaceIndex: 1,
    usedPercent,
  };
  return new InterfaceUsageReport(json);
}

test('test interface off', () => {
  const interfaceThroughput = new InterfaceThroughput(
    createReading(AdminStatus.DOWN, OperStatus.DOWN),
    createUsageReport(90.0, 1),
  );
  expect(interfaceThroughput.isError()).toBe(false);
  expect(interfaceThroughput.isRunning()).toBe(false);
  expect(interfaceThroughput.getState()).toBe(State.OFF);
  expect(interfaceThroughput.getDescription()).toBe('OFF');
  expect(interfaceThroughput.isLimitExceeded()).toBe(false);
});

test('test interface down error', () => {
  const interfaceThroughput = new InterfaceThroughput(
    createReading(AdminStatus.UP, OperStatus.DOWN),
    createUsageReport(100.0, 1),
  );
  expect(interfaceThroughput.isError()).toBe(true);
  expect(interfaceThroughput.isRunning()).toBe(false);
  expect(interfaceThroughput.getState()).toBe(State.ERROR);
  expect(interfaceThroughput.getDescription()).toBe('DOWN');
  expect(interfaceThroughput.isLimitExceeded()).toBe(true);
});

test('test reading before usage report', () => {
  const interfaceThroughput = new InterfaceThroughput(
    createReading(AdminStatus.UP, OperStatus.UP),
    createUsageReport(100.0, 3),
  );
  expect(interfaceThroughput.isError()).toBe(false);
  expect(interfaceThroughput.isRunning()).toBe(true);
  expect(interfaceThroughput.getState()).toBe(State.OK);
  expect(interfaceThroughput.getDescription()).toBe('OK');
  expect(interfaceThroughput.isLimitExceeded()).toBe(false);
});
