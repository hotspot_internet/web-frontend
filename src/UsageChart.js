import React from 'react';
import axios from 'axios';
import IconButton from '@material-ui/core/IconButton';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import ArrowForwardIcon from '@material-ui/icons/ArrowForward';
import NetworkUsage from './NetworkUsage';
import DataLimit from './DataLimit';
import { InterfaceActivityCollection, Link } from './InterfaceActivityCollection';
import config from './config';
import SimpleAlert from './SimpleAlert';
import InterfaceActivityModel from './InterfaceActivityModel';
import ProgressBackdrop from './ProgressBackdrop';

function createUrl(interfaceIndex: number, start: Date): String {
  const parameters = `?start=${start.toISOString()}&period=MONTH&divided=true`;
  return `${config.interfaceUrl}/${interfaceIndex}${parameters}`;
}

function getDataLimitUrl(interfaceIndex: number): String {
  return `${config.dataLimitUrl}/${interfaceIndex}`;
}

function getErrorMessage(requestError: Object): String {
  if (requestError.response && requestError.response.data) {
    return requestError.response.data;
  }
  return requestError.message;
}

function floatStyle(float: String): Object {
  return ({
    float,
  });
}

export default class UsageChart extends React.Component {
  hasRequestedInitialActivities: boolean

  constructor(props) {
    super(props);
    this.hasRequestedInitialActivities = false;
    this.state = {
      dataLimits: [],
      error: null,
      activityCollections: [],
      isDataLimitPending: true,
      isActivityPending: false,
    };
  }

  componentDidMount() {
    const promises = [];
    for (let index = 1; index <= config.maxInterfaceIndex; index += 1) {
      promises.push(this.updateDataLimit(index));
    }
    Promise
      .all(promises)
      // eslint-disable-next-line no-unused-vars
      .then((completedPromises) => {
        this.setState({
          isDataLimitPending: false,
        });
      });
  }

  // eslint-disable-next-line no-unused-vars
  componentDidUpdate(prevProps, prevState, snapshot) {
    const { dataLimits } = this.state;
    if (dataLimits.length === config.maxInterfaceIndex
      && !this.hasRequestedInitialActivities) {
      this.hasRequestedInitialActivities = true;
      const nowDate = new Date();
      this.updateActivities(
        dataLimits
          .map((dataLimit) => createUrl(
            dataLimit.interfaceIndex,
            dataLimit.getCycleStartDate(nowDate),
          )),
      );
    }
  }

  handleLinkName(link: String) {
    const { activityCollections } = this.state;
    this.updateActivities(activityCollections
      .map((collection) => collection.getLinkHref(link)));
  }

  getActivityArrays() {
    const { activityCollections } = this.state;
    return activityCollections
      .map((collection: InterfaceActivityCollection) => {
        const { activityModels } = collection;
        return activityModels.map((model: InterfaceActivityModel) => model.interfaceActivity);
      });
  }

  getNetworkUsages(): NetworkUsage[] {
    const networkUsages = [];
    const activityArrays = this.getActivityArrays();
    if (activityArrays.length === 0) {
      return networkUsages;
    }

    for (let usageIndex = 0; usageIndex < activityArrays[0].length; usageIndex += 1) {
      const usagesToSum = activityArrays.map((usages) => usages[usageIndex]);
      networkUsages.push(NetworkUsage.sumNetworkUsages(usagesToSum));
    }
    return networkUsages;
  }

  clearError() {
    this.setState({
      error: null,
    });
  }

  isRequestPending(): boolean {
    const { isDataLimitPending, isActivityPending } = this.state;
    return isDataLimitPending || isActivityPending;
  }

  updateDataLimit(interfaceIndex: number): Promise {
    const url = getDataLimitUrl(interfaceIndex);
    return axios.get(url)
      .then((response) => {
        const { dataLimits } = this.state;
        const receivedDataLimits = response.data;
        if (receivedDataLimits.length > 1) {
          // expected only 1 data limit for interface but received multiple;
          // may eventually log this as an error or something
        } else if (receivedDataLimits.length === 0) {
          throw new Error(`No data limits received for interface ${interfaceIndex}`);
        }
        dataLimits.push(new DataLimit(receivedDataLimits[0]));
        this.setState({
          dataLimits,
        });
      })
      .catch((requestError) => {
        this.setState({
          error: getErrorMessage(requestError),
        });
      });
  }

  updateActivities(urls: [String]) {
    this.setState({
      isActivityPending: true,
    });
    Promise
      .all(urls.map(this.updateActivity.bind(this)))
      // eslint-disable-next-line no-unused-vars
      .then((completedPromises) => {
        this.setState({
          isActivityPending: false,
        });
      });
  }

  updateActivity(url: String): Promise {
    return axios.get(url)
      .then((response) => {
        // handle success
        let { activityCollections } = this.state;
        if (activityCollections.length === config.maxInterfaceIndex) {
          activityCollections = [];
        }
        activityCollections.push(new InterfaceActivityCollection(response.data));
        this.setState({
          activityCollections,
          error: null,
        });
      })
      .catch((requestError) => {
        this.setState({
          error: getErrorMessage(requestError),
        });
      });
  }

  renderError() {
    const { error } = this.state;
    return error !== null
      ? <SimpleAlert alertMessage={error} onClose={() => this.clearError()} />
      : <></>;
  }

  renderForwardBackButtons() {
    return (
      <>
        <div style={floatStyle('left')}>
          <IconButton onClick={() => this.handleLinkName(Link.PREVIOUS)}>
            <ArrowBackIcon />
          </IconButton>
          Previous
        </div>
        <div style={floatStyle('right')}>
          Next
          <IconButton onClick={() => this.handleLinkName(Link.NEXT)}>
            <ArrowForwardIcon />
          </IconButton>
        </div>
      </>
    );
  }

  renderProgressBackdrop() {
    return (<ProgressBackdrop open={this.isRequestPending()} />);
  }
}
