export default class InterfaceUsageReport {
  instant: Date
  interfaceIndex: number
  usedPercent: number

  constructor(json: Object) {
    this.instant = new Date(json.instant);
    this.interfaceIndex = json.interfaceIndex;
    this.usedPercent = json.usedPercent;
  }

  isDataLeft(): boolean {
    return this.usedPercent < 100;
  }

  static chronologicalSorter(report1: InterfaceUsageReport, report2: InterfaceUsageReport): number {
    return report1.instant.getTime() - report2.instant.getTime();
  }
}
